export class Property {

public static apiRoute: '/properties';

id       : number;
address1 : string;
address2 : string;
city     : string;
postcode : string;
price    : number;
bedrooms : number;
bathrooms: number;

constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
