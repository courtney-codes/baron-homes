import { NavigationComponent } from './navigation/navigation.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '',
    component: NavigationComponent,
    data: {
      title: 'Home'
    },
    children: [
      // Home/dashboard component
      {
        path: 'home',
        data: {
          title: ''
        },
        component: HomeComponent
      },
      {
      path: 'properties',
      data: {
          title: 'Properties'
        },
        loadChildren: './properties/property.module#PropertyModule'
      },
      {
      path: 'tenants',
      data: {
          title: 'Tenants'
        },
        loadChildren: './tenants/tenant.module#TenantModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'home'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
