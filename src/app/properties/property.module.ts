import { PropertyRouting } from './property.routing';
import { PropertyListComponent } from './property-list/property-list.component';
import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular2-datatable';
import { CommonModule } from '@angular/common';
import { PropertyDetailComponent } from './property-detail/property-detail.component';
import { PropertyCardComponent } from './property-card/property-card.component';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    PropertyRouting
  ],
  declarations: [
    PropertyDetailComponent,
    PropertyListComponent,
    PropertyCardComponent
  ]
})
export class PropertyModule { }
