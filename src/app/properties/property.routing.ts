import { PropertyDetailComponent } from './property-detail/property-detail.component';
import { PropertyListComponent } from './property-list/property-list.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'list',
    data: {
      title: 'Property List'
    },
    children: [
      {
        path: '',
        component: PropertyListComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'detail/:id',
        component: PropertyDetailComponent,
        data: {
          title: 'Property Details'
        }
      },
      {
        path: 'edit',
        component: PropertyDetailComponent,
        data: {
          title: 'Edit Property'
        }
      }
    ]
  },
  {
    path: 'editor',
    component: PropertyDetailComponent,
    data: {
      title: 'Edit Property'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class PropertyRouting { }
