import { Property } from 'classes/property';
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { PropertyCardComponent } from './../property-card/property-card.component';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.scss']
})
export class PropertyListComponent implements OnInit {

  propertyData    = [
    { id: 1, houseNo: 'Flat 2', address1: '42 Brunswick Place', address2: '', town: 'Hove', postcode: 'BN3 1NA', image: 'house1', price: 1350},
    { id: 2, houseNo: '8', address1: 'Charis Court', address2: 'Eaton Road', town: 'Hove', postcode: 'BN3 3PQ', image: 'house2', price: 1500},
    { id: 3, houseNo: '22a', address1: 'East Street', address2: '', town: 'Brighton', postcode: 'BN1 1HL', image: 'house3', price: 3275},
    { id: 4, houseNo: 'Flat 2', address1: '42 Brunswick Place', address2: '', town: 'Hove', postcode: 'BN3 1NA', image: 'house4', price: 1400}
];

  constructor() {
  }

  ngOnInit() {
  }




}
