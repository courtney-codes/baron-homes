import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceDetailComponent } from 'app/modules/maintenance/maintenance-detail/maintenance-detail.component';
import { MaintenanceRequestsComponent } from 'app/modules/maintenance/maintenance-requests/maintenance-requests.component';

const routes: Routes = [
    {
      path: 'list',
      data: {
        title: 'Maintenance List'
      },
      children: [
        {
          path: '',
          component: MaintenanceRequestsComponent,
          data: {
            title: 'Maintenance Requests'
          }
        },
        {
          path: 'detail/:id',
          component: MaintenanceDetailComponent,
          data: {
            title: 'Request Details'
          }
        }
      ]
    }
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
  })
export class MaintenanceRoutingModule { }
