import { DataTableModule } from 'angular2-datatable';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceRequestsComponent } from './maintenance-requests/maintenance-requests.component';
import { MaintenanceDetailComponent } from './maintenance-detail/maintenance-detail.component';
import { MaintenanceListComponent } from './maintenance-list/maintenance-list.component';
import { MaintenanceRoutingModule } from 'app/modules/maintenance/maintenance.routing';

@NgModule({
    imports: [
        CommonModule,
        MaintenanceRoutingModule,
        DataTableModule
    ],
    declarations: [
        MaintenanceRequestsComponent,
        MaintenanceDetailComponent,
        MaintenanceListComponent
    ]
})
export class MaintenanceModule { }
