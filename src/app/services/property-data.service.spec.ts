import { TestBed, inject } from '@angular/core/testing';

import { PropertyDataService } from './property-data.service';

describe('PropertyDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PropertyDataService]
    });
  });

  it('should be created', inject([PropertyDataService], (service: PropertyDataService) => {
    expect(service).toBeTruthy();
  }));
});
