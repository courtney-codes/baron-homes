import { AsyncSubject } from 'rxjs/AsyncSubject';
import { BaronDataObject } from 'app/classes/baron-data-object';

export class Property {

apiRoute = '/properties/';

id       : number;
address1 : string;
address2 : string;
town     : string;
postcode : string;
price    : number;
bedrooms : number;
bathrooms: number;

GscDate          : string;
GscNextDue       : string;
electricalDate   : string;
electricalNextDue: string;
EpcDate          : string;
EpcNextDue       : string;


constructor(values: Object = {}) {
    Object.assign(this, values);
}

}
