import { TenantEditComponent } from './tenant-edit/tenant-edit.component';
import { TenantSearchComponent } from './tenant-search/tenant-search.component';
import { TenantProfileComponent } from './tenant-profile/tenant-profile.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'search',
    data: {
      title: 'Search'
    },
    children: [
      {
        path: '',
        component: TenantSearchComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'profile/:id',
        component: TenantProfileComponent,
        data: {
          title: 'Tenant Profile'
        }
      },
      {
        path: 'edit',
        component: TenantEditComponent,
        data: {
          title: 'Edit Tenants'
        }
      }
    ]
  },
  {
    path: 'editor',
    component: TenantEditComponent,
    data: {
      title: 'Edit Tenants'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})

export class TenantRouting { }

