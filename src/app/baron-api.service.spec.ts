import { TestBed, inject } from '@angular/core/testing';

import { BaronApiService } from './baron-api.service';

describe('BaronApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BaronApiService]
    });
  });

  it('should be created', inject([BaronApiService], (service: BaronApiService) => {
    expect(service).toBeTruthy();
  }));
});
